package com.captech.retrorx.model;

import java.util.ArrayList;

/**
 * Created by ptrocki on 19/02/16.
 */

/**
 * All subreddit data
 */
public class Subreddit
{
    String kind;
    Data data;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Subreddit.Data getData() {
        return data;
    }

    public void setData(Subreddit.Data data) {
        this.data = data;
    }

    /**
     * Subreddit news
     */
    public class Data {
        ArrayList<News> children;

        public ArrayList<News> getChildren() {
            return children;
        }

        public void setChildren(ArrayList<News> children) {
            this.children = children;
        }
    }

}

