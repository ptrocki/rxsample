package com.captech.retrorx.model;

/**
 * Created by ptrocki on 19/02/16.
 */
/**
 * Simple news
 */
public class News {

    String kind;

    Data data;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    /**
     * News properties
     */
    public class Data{

        String subreddit_id;

        String title;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSubreddit_id() {
            return subreddit_id;
        }

        public void setSubreddit_id(String subreddit_id) {
            this.subreddit_id = subreddit_id;
        }
    }
}
