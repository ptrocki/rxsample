package com.captech.retrorx.view;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.captech.retrorx.R;
import com.captech.retrorx.model.News;
import com.captech.retrorx.model.Subreddit;

import java.util.ArrayList;

/**
 * Created by ptrocki on 15/03/16.
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    private ArrayList<News> mDataset;

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView title;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tv_title);
        }
    }

    public NewsAdapter() {
        this.mDataset = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.i("Adapter ", String.valueOf(mDataset.get(position).getData().getTitle()));
        holder.title.setText(mDataset.get(position).getData().getTitle());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void setData(Subreddit subreddit){
        mDataset.addAll(subreddit.getData().getChildren());
        notifyDataSetChanged();
    }

}