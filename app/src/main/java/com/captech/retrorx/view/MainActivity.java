package com.captech.retrorx.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.captech.retrorx.R;
import com.captech.retrorx.RxApplication;
import com.captech.retrorx.model.Subreddit;
import com.captech.retrorx.network.NetworkService;
import com.captech.retrorx.presenter.PresenterInteractor;
import com.captech.retrorx.presenter.PresenterLayer;

import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    private static final String EXTRA_RX = "EXTRA_RX";

    private NetworkService service;

    private boolean rxCallInWorks = false;

    private PresenterInteractor presenter;

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private NewsAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        service = ((RxApplication)getApplication()).getNetworkService();
        presenter = new PresenterLayer(this, service);
        if(savedInstanceState!=null){
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new NewsAdapter();
        mRecyclerView.setAdapter(mAdapter);

        presenter.loadRetroData();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.rxUnSubscribe();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_RX, rxCallInWorks);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(rxCallInWorks)
            presenter.loadRxData();
    }

    //TODO implement
    public void showRxResults(Subreddit response){
        mAdapter.setData(response);
    }

    public void showRxFailure(Throwable throwable){
        Log.d("TAG", throwable.toString());

    }

    public void showRetroResults(Response<Subreddit> response){
        mAdapter.setData(response.body());
    }

    public void showRetroFailure(Throwable throwable){
        Log.d("TAG", throwable.toString());

    }

    public void showRxInProcess(){

    }

    public void showRetroInProcess(){

    }

}
