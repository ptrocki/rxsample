package com.captech.retrorx;

import android.app.Application;

import com.captech.retrorx.network.NetworkService;

/**
 * Created by cteegarden on 1/26/16.
 */
public class RxApplication extends Application {

    private NetworkService networkService;
    @Override
    public void onCreate() {
        super.onCreate();

        networkService = NetworkService.getNetworkService();

    }
    public NetworkService getNetworkService(){
        return networkService;
    }

}
