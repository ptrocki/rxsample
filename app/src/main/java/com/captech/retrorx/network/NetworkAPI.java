package com.captech.retrorx.network;

import com.captech.retrorx.model.Subreddit;

import retrofit2.Call;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by ptrocki on 19/02/16.
 */
public interface NetworkAPI {

        @GET("androiddev.json")
        Call<Subreddit> getSubreddit();

        @GET("androiddev.json")
        Observable<Subreddit> getSubredditObservable();
}
