package com.captech.retrorx.presenter;

import com.captech.retrorx.model.Subreddit;
import com.captech.retrorx.network.NetworkService;
import com.captech.retrorx.view.MainActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Observer;
import rx.Subscription;

/**
 * Created by cteegarden on 1/28/16.
 */
@SuppressWarnings("unchecked")
public class PresenterLayer implements PresenterInteractor {

    private MainActivity view;
    private NetworkService service;
    private Subscription subscription;

    public PresenterLayer(MainActivity view, NetworkService service){
        this.view = view;
        this.service = service;
    }

    public void loadRxData(){
        view.showRxInProcess();
        Observable<Subreddit> friendResponseObservable = (Observable<Subreddit>)
                service.getPreparedObservable(service.getAPI().getSubredditObservable(), Subreddit.class, true, true);
        subscription = friendResponseObservable.subscribe(new Observer<Subreddit>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                view.showRxFailure(e);
            }

            @Override
            public void onNext(Subreddit subreddit) {
                view.showRxResults(subreddit);
            }

        });
    }

    public void loadRetroData(){
        view.showRetroInProcess();
        Call<Subreddit> call = service.getAPI().getSubreddit();
        call.enqueue(new Callback<Subreddit>() {


            @Override
            public void onResponse(Response<Subreddit> response) {
                view.showRetroResults(response);
            }

            @Override
            public void onFailure(Throwable t) {
                view.showRetroFailure(t);
            }

        });
    }

    public void rxUnSubscribe(){
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();
    }


}
