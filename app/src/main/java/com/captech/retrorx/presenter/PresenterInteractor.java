package com.captech.retrorx.presenter;

/**
 * Created by cteegarden on 2/1/16.
 */
public interface PresenterInteractor {
    void loadRxData();
    void loadRetroData();
    void rxUnSubscribe();
}
